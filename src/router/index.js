import Vue from 'vue';
import Router from 'vue-router';

import Users from '@/components/Users/Index';
import List from '@/components/Users/List';
import Single from '@/components/Users/Single';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      component: Users,
      children: [
        {
          path: '',
          name: 'list',
          component: List,
          meta: {
            title: 'Список',
          },
        },
        {
          path: '/user/:id',
          name: 'user',
          component: Single,
          meta: {
            title: 'Пользователь',
          },
        },
      ],
    },
  ],
});
