export default {
  list: state => state.users,
  user: state => state.user,
  previousPageUrl: state => state.pageUrl.previous,
  nextPageUrl: state => state.pageUrl.next,
  loading: state => state.loading,
};
