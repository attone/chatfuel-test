import * as TYPES from './types';

export default {
  [TYPES.SET_USERS_LIST](state, data) {
    state.users = data;
  },
  [TYPES.SET_USER](state, data) {
    state.user = data;
  },
  [TYPES.SET_LOADING](state, isLoading) {
    state.loading = isLoading;
  },
  [TYPES.SET_NEXT_PAGE](state, url) {
    state.pageUrl.next = url;
  },
  [TYPES.SET_PREVIOUS_PAGE](state, url) {
    state.pageUrl.previous = url;
  },
};
