export const SET_USERS_LIST = 'SET_USERS_LIST';
export const SET_USER = 'SET_USER';
export const SET_LOADING = 'SET_LOADING';
export const SET_NEXT_PAGE = 'SET_NEXT_PAGE';
export const SET_PREVIOUS_PAGE = 'SET_PREVIOUS_PAGE';
