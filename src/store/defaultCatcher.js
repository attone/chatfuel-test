import Vue from 'vue';

export default (commit, response) => {
  Vue.$notify({
    title: 'Ошибка',
    classes: 'error',
    text: response,
  });
};
