import { http } from '../api';
import * as types from './types';
import defaultCatcher from './defaultCatcher';

export default {
  async getUsers({ commit }, page = '') {
    commit(types.SET_LOADING, true);

    try {
      const response = await http.get(`/users?per_page=6&${page}`);
      commit(types.SET_USERS_LIST, response.data.data);
      commit(types.SET_NEXT_PAGE, response.data.nextPageUrl);
      commit(types.SET_PREVIOUS_PAGE, response.data.previousPageUrl);
    } catch ({ response }) {
      defaultCatcher(commit, response.data.message);
    }

    commit(types.SET_LOADING, false);
  },

  async getCurrentUser({ commit }, id) {
    commit(types.SET_LOADING, true);
    commit(types.SET_USER, null);

    try {
      const response = await http.get(`/users/${id}`);
      commit(types.SET_USER, response.data.data);
    } catch ({ response }) {
      defaultCatcher(commit, response.data.message);
    }

    commit(types.SET_LOADING, false);
  },

  async updateUser({ commit }, user, id) {
    commit(types.SET_LOADING, true);

    try {
      const response = await http.post(`/users/${id}`, user);
      commit(types.SET_USER, response.data.data);
    } catch ({ response }) {
      defaultCatcher(commit, response.data.message);
    }

    commit(types.SET_LOADING, false);
  },
};
