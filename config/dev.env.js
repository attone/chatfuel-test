'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
//  TODO: paste development server
//  APP_API_URL: "'YOU_SERVER'",
})
